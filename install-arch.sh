#!/bin/zsh

# Use ntp to set the system time
timedatectl set-ntp true

# Create a boot directory
mkdir /mnt/boot

# to create the partitions programatically (rather than manually)
# we're going to simulate the manual input to fdisk
# The sed script strips off all the comments so that we can 
# document what we're doing in-line with the actual commands
# Note that a blank line (commented as "defualt" will send a empty
# line terminated with a newline to take the fdisk default.
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk ${TGTDEV}
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # default - start at beginning of disk 
  +100M # 100 MB boot parttion
  n # new partition
  p # primary partition
  2 # partion number 2
    # default, start immediately after preceding partition
    # default, extend partition to end of disk
  a # make a partition bootable
  1 # bootable partition is partition 1 -- /dev/sda1
  p # print the in-memory partition table
  w # write the partition table
  q # and we're done
EOF

# Format the newly created partitions
mkfs.ext3 /dev/sda1
mkfs.ext4 /dev/sda2

# Mount the partitions to the live system
mount /dev/sda1 /mnt
mount /dev/sda2 /mnt/boot

# Set up pacman mirrors
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
awk '/^## Sweden$/{f=1}f==0{next}/^$/{exit}{print substr($0, 2)}' /etc/pacman.d/mirrorlist.backup
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist

# Install arch
pacstrap /mnt base

# Genereate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Change root into the newly created system
arch-chroot /mnt

# Set the local timezone
ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime
hwclock --systohc

# Create and set the locale of the new system
echo en_US.UTF-8 > /etc/locales.gen
echo sv_SE.UTF-8 >> /etc/locales.gen
locale-gen
touch /etc/locale.conf
echo LANG=sv_SE.UTF-8 > /etc/locale.conf

# Set the language of the virtual console
touch /etc/vconsole.conf
echo KEYMAP=sv-latin1 > /etc/vconsole.conf

# Create and set the hostname for the new system
touch /etc/hostname
echo dinaria > /etc/hostname
echo 127.0.0.1	localhost > /etc/hosts
echo ::1 localhost >> /etc/hosts
echo 127.0.1.1 dinaria.localdomain dinaria >> /etc/hosts

# Set a root password for the new system
passwd

# Install vim, because it is needed
pacman -S neovim

# Install and configure GRUB
pacman -S grub
grub-install --target=i386-pc /dev/sda

# Exit the new system back to the live system
exit

# Unmount all partations
umount -R /mnt

# Reboot the system
reboot